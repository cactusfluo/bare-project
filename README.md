# Bare Necessities Project

The "Bare Necessities Project" is a project which aims to promote (and build) a
**greener** and **sounder** web.

To build our project website, we use the open-source static site generator
[Hugo](https://gohugo.io).
