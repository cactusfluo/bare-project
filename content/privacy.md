---
title: "Privacy"
date: 2018-11-07T10:46:27+01:00
draft: false

icon: "&#x1f512;"
---

As we promote a **greener** and **sounder** web, we do not use any **cookie**
or **advertising service** and do not get any of your **data**.
