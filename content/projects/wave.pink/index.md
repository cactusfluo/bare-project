---
title: "Wave.pink"
date: 2018-11-04T10:15:12+01:00
draft: false

websiteUrl: "http://wave.pink"
creator: "Bare Necessities Project"
description: "An audio streaming/downloading website"
contactName: "Sebastien Huertas"
contactEmail: "cactusfluo@laposte.net"
icon: "&#x1f30a;"
status: "work in progress"
---

Video streaming in the most consuming entertainment medium on internet. It's
pollution is huge.
The aim of this project would be to offer another entertainment medium. An
ecological one.
We're trying to make an audio streaming or downloading platform to **listen** 
movies and series.
