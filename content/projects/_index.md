---
title: "Bare Projects"
date: 2018-11-02T19:56:04+01:00
draft: false

icon: "&#x1f30d;"
---

These projects are not necessarly linked to the **Bare Necessities Project**, it
is just a non-exhaustive list of what we call **Bare Projects**.
