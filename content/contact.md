---
title: "Contact"
date: 2018-11-02T19:57:50+01:00
draft: false

icon: "&#x1f4e7;"
---

You want to get the **Bare Label**, become a **Bare Necessities Project**
partner, create a new **Bare project** or simply contact us for anything else ?
Please contact us at *contact@bare-project.org*

* * *

Sebastien Huertas<br>
cactusfluo@laposte.net
