---
title: "Bare Label"
date: 2018-11-13T12:14:39+01:00
draft: false

icon: "&#x1f331;"
description: "The standard Bare Label which says that your site is bare, honnest
to its viewers, only keeps the essential and pollutes way less than a standard 
website."
---

<!-- LABEL UNAVAILABLE -->

* * *

> The aim of the Bare Label is to says that your site is bare, honnest to its
viewers, only keeps the essential and pollutes way less than a standard website.

* * *

We are in a **research phase** which will allow us to have a clear view, among
other things, of what are the most **polluting/consuming** things on the
internet. Following this phase, we will be able to define the
**Bare Label rules**.

<!-- LABEL AVAILABLE -->

<!--
* * *

> The Bare Label says that your site is bare, honnest to its viewers, only keeps
the essential and pollutes way less than a standard website.

* * *

{{<
	img
	title="bare label"
	src="images/logo.svg"
	width="200px"
>}}

A website which has the **Bare Label** is a site which is **green** and
**sound**.

By **green**, we mean that this is a website which is designed to consume the
lowest energy possible, on your terminal (computer, smartphone etc.) but also
on the datacenter that hosts it.
This choice was made cause we (**The Bare Necessities Project**) found that
**technology pollution** was mainly made during the **production phase**. So, to
reduce this phase's pollution, we thought that **preserving our terminals or
datacenters** was a great idea to **increase their lifespan** and so **reduce
their production frequency**.
To do so, Bare Labelled websites avoid **videos**, **heavy images** or even
**advertising services** which all are a really important part of these heavy
calculations which damage our terminals or datacenters.

By **sound**, we mean that this is a website which **keeps the essential**, does
**not sell your data or your attention**.
To do so, they does not use any **advertising services** or any **server-side
additional calculation** (it does not track you, get informations about you or
anything else). Once the site is online, the only thing it does is send you
fixed web pages.

The labelled websites can opt for **additional rules** (called **options**),
meaning that the partner **commits even more** to a **better internet**.
These **options** can be found on the upper left side of the label icon.

* * *

### [Here are the Bare Label icon and its options]({{< ref "/label" >}}/icon)

### [Here are the Bare Label rules]({{< ref "/label/rules" >}})

* * *

If you want to get the **Bare Label** for your website, please
[contact us]({{< ref "/contact" >}})
-->
