---
title: "No Advertising"
date: 2018-11-15T14:58:28+01:00
draft: false

rule: "Your website cannot use any kind of advertising service."
---

### EXPLANATION

As a **Bare Website** must be **sound**, selling user **data** or **attention**
is prohibited. Using **advertising services** would make you an **attention
seller**, this is why it is not allowed.

### ALTERNATIVE

The most **honnest** and **ethic** way to earn money from your website is by
**donation**.
