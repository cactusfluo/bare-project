---
title: "Minification"
date: 2018-11-15T14:58:54+01:00
draft: false

rule: "Your website's script files must be minified."
---

#### EXPLANATION

Your **HTML/CSS/JS** files must be **minified**. It means that every useless 
**space**, **carriage return** or any **comment** must be removed from your
**published** site code. In this way, the transfered code (from the server to
the user) will be **lighter** and **faster**. The economy of bytes can be of
**15% / 20%**.

#### GUIDE

You can easily find a **minifier** program which will process your files to
**minify** them. I personally use my website generator **Hugo** minifier which
do it automatically when I build the site:

	$ hugo --minify

I also used the unix program called **minify** which handles **HTML/CSS/JS**
(and more) files:

	$ minify my_file.html > my_minified_file.html
	$ minify my_file.css > my_minified_file.css
	$ minify my_file.js > my_minified_file.js

Here's a script I used with the **minify** program which allows you to minify an
entire project directory. It get each of your **HTML/CSS/JS** files recursively
and use **minify** to minify them.
Of course, it must be used on your built website cause it will make your code
**unreadable for development**: 
[minifier.sh]({{< ref "/label/rules" >}}/ressources/minifier.sh)

	$ ./minifier.sh path/to/my/website/directory/

One another good trick to save some worthlessly calculation would be to have
**only one CSS file**. In this way, only **one query** is needed to get the
styling part. Even more, once the **CSS loaded** and **cached** by the
browser, there will be no additional query to style pages since it is already
here. Here is a command line to **concatenate** multiple **CSS** files into
one file:

	$ cat header.css footer.css main.css > style.css
