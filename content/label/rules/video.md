---
title: "Video"
date: 2018-11-14T17:07:56+01:00
draft: false

rule: "Your website cannot contain any video."
---

### EXPLANATION

**Video streaming** is known to be a massive actor on internet pollution. It's
**over half** of the entire internet traffic. We think that video is **not
indispensable** and most of your content can be shown via **text and image**.
That's why videos are prohibited on **Bare Labelled websites**.
