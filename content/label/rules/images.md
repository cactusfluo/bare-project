---
title: "Images"
date: 2018-11-14T17:07:43+01:00
draft: false

rule: "Your website's images cannot exceed 100ko."
---

### EXPLANATION

High definition images are really heavy and so, power consuming and polluting.
That's why **Bare Labelled** websites images cannot exceed **100ko**.

- **100ko:** maximum
- **70ko**: great
- **50ko**: really really good
- **30ko**: perfect

* * *

### ALTERNATIVE

#### PIXELATED IMAGES

##### RETRO LOOK
Here is how we see images on an ideal web, you can read it as tips to reduce
your images size (the **order** of the modifications can be important).

- Resize your images to max **800x600px**
- Use **4 colors** images
- Use **png** format (no quality loss)

To performe these image processing, we use the **convert** program from
**ImageMagick**, here is our command line:

	$ convert original_image.png -resize 800x600 -colors 4 new_image.png

To display these processed images on your site, you should disable the **image
anti-aliasing** which blures them. Here's the **CSS** parameters to do so:
(this does not currently work on **EDGE**)

	img { 
		image-rendering: optimizeSpeed;             /* STOP SMOOTHING, GIVE ME SPEED */
		image-rendering: -moz-crisp-edges;          /* Firefox */
		image-rendering: -o-crisp-edges;            /* Opera */
		image-rendering: -webkit-optimize-contrast; /* Chrome (and eventually Safari) */
		image-rendering: pixelated; 				/* Chrome */
		image-rendering: optimize-contrast;         /* CSS3 Proposed */
		-ms-interpolation-mode: nearest-neighbor;   /* IE8+ */
	}

Here are some examples of this **RETRO LOOK** processed images:

{{<
	img
	title="pigeon retro"
	src="/label/rules/ressources/images/pigeon.png"
	legend="69ko"
>}}

{{<
	img
	title="gorilla retro"
	src="/label/rules/ressources/images/gorilla.png"
	legend="55.8ko"
>}}

{{<
	img
	title="octopus retro"
	src="/label/rules/ressources/images/octopus.png"
	legend="54.2ko"
>}}

##### LOSSY COMPRESSION IMAGES
Another alternative would be to use **lossy compression image format** like
**JPEG**. It allows you to show clear images without needing an important size.
Of course, by being lossy, the compression can **damage** your image but in most
of the case, it is way enough.

Here is an example of how you can proceed image conversion to **JPEG**:

	$ convert original_image.png new_image.jpg

And here's how to reduce **JPEG quality** to reduce its size ('`X`' being the 
quality from 1 to 100)

	$ convert origin_image.jpg -quality X new_image.jpg

Using this command line, I also made a **BASH** script for you which handles
this image reduction finding the matching '`X`' value: 
[lighter.sh]({{< ref "/label" >}}/rules/ressources/lighter.sh)<br>
You call it like this to reduce the image size to max 80ko:

	./lighter.sh original_image.jpg destination_image.jpg 80

{{<
	img
	title="pigeon jpeg"
	src="/label/rules/ressources/images/pigeon.jpg"
	legend="75.5ko"
>}}

{{<
	img
	title="gorilla jpeg"
	src="/label/rules/ressources/images/gorilla.jpg"
	legend="82.9ko"
>}}

{{<
	img
	title="octopus jpeg"
	src="/label/rules/ressources/images/octopus.jpg"
	legend="45.3ko"
>}}

##### DRAWINGS
The best option to reduce drasticailly your image weight is by drawing them
yourself using only 3-4 colors. Cause if the image is already made to be
**800x600px** and to use only **4 colors**, then, the conversion will be
greater.

{{<
	img
	title="pigeon drawing"
	src="/label/rules/ressources/images/drawing_pigeon.png"
	legend="22.8ko"
>}}

{{<
	img
	title="gorilla drawing"
	src="/label/rules/ressources/images/drawing_gorilla.png"
	legend="24.2ko"
>}}

{{<
	img
	title="octopus drawing"
	src="/label/rules/ressources/images/drawing_octopus.png"
	legend="30.9ko"
>}}

#### VECTOR

##### SVG
Vectorial images are special as they can be **infinitly scaled up** and can be
**really light**. It is an excellent choice for **drawings**, **sketches**,
**diagrams**, **logos** etc as long as you do not want to print photographs.
it **must be drawn**. The most known vectorial image format is **SVG**.

{{<
	img
	title="logo"
	src="images/logo.svg"
	legend="16.4ko"
	width="150px"
>}}

##### CSS
At the same level, there are **CSS drawings**. Colored and gradient backgrounds
can easily be handled with **CSS** instead of heavy images. More sophisticated
shapes can also be done with it like a logo. As **SVG** images, it can be
**infinitly scaled up**.

#### UNICODE
This is a trick. Some icons on your website can easily be printed with
**unicode characters** (emoticons). As it is already loaded on the user's
computer, it **does not require any calculation** and it is **fast**.
Of course, these icons can vary between **OS**, **computers** or even
**browsers** but the message will stay the same.
