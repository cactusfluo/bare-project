#!/usr/bin/env bash
################################################################################
# by cactusfluo
################################################################################
# Description:
#  "minifier" looks for HTML/CSS/JS files to minify them. If a directory is
#  given, then, it will recursively find HTML/CSS/JS files in it.
################################################################################
# Usage:
#  > "minifier" FILE | DIR [FILE | DIR]...
#
#  . FILE: A HTML/CSS/JS file path.
#  . DIR: A project directory path which contains HTML/CSS/JS files.
################################################################################

if [[ -t 1 ]]; then
	C_RED="\033[31;01m"
	C_GREEN="\033[32;01m"
	C_YELLOW="\033[33;01m"
	C_BLUE="\033[34;01m"
	C_PINK="\033[35;01m"
	C_CYAN="\033[36;01m"
	C_NO="\033[0m"
fi

################################################################################
###                                FUNCTIONS                                 ###
################################################################################

function		failure()
{
	printf "${C_RED}ERROR: ${C_NO}${1}\n" >&2
}

function		success()
{
	printf "${C_GREEN}SUCCESS: ${C_NO}${1}\n"
}

function		process_file()
{
	ext=${1##*.}
	name=${1%.*}

	if [[ ${ext} == html ]] || [[ ${ext} == css ]] || [[ ${ext} == js ]]; then
		minify ${1} > .minifier
		if [[ $? -ne 0 ]]; then
			failure "Cannot minify [${1}]\n"
		fi
		cat .minifier > "${1}"
		printf "[${1}]\n"
	fi
}

function		process_dir()
{
	files=(${1}/*)

	for file in "${files[@]}"; do
		if [[ -f ${file} ]]; then
			process_file "${file}"
		elif [[ -d ${file} ]]; then
			process_dir "${file}"
		fi
	done
}

################################################################################
###                                   MAIN                                   ###
################################################################################

### CHECK MINIFY
which minify > /dev/null
if [[ $? -ne 0 ]]; then
	failure "\"minify\" binary installation is required to run \"minifier\""
	exit 1
fi

### CHECK USER
printf "${C_RED}press ENTER to confirm minification...${C_NO}"
read

### LOOP ON FILES
for file in ${@#}; do
	if [[ -f ${file} ]]; then
		process_file "${file}"
	elif [[ -d ${file} ]]; then
		process_dir "${file}"
	else
		failure "[${file}] is not a regular file or a directory."
	fi
done

### REMOVE TEMPORARY FILE
rm -f .minifier
