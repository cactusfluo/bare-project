#!/usr/bin/env bash
################################################################################
# by cactusfluo
################################################################################
# USAGE:
#  > ./lighter.sh IMAGE_SRC IMAGE_DST MAX_SIZE
#   - IMAGE_SRC: Original image (ex: "original_image.jpg")
#   - IMAGE_DST: Resulting image name (ex: "resulting_image.jpg")
#   - MAX_SIZE:  Maximum size of the resulting image in ko (ex: "50" for 50ko)
################################################################################
# NOTE TO WINDOWS USERS:
#  On windows, an already existing "convert" program can create some issues.
#   To avoid this issue, you have 2 possibilities:
#
#    1: Put you "image-magick" path before the other "convert" path:
#       'PATH="my_image_magick_path:${PATH}"'
#       instead of
#       'PATH="${PATH}:my_image_magick_path"'
#    2: Simply put the following line in this script just after this comment
#        part:
#        'CONVERT_PATH="my_image_magick_path"'
################################################################################

[[ ${CONVERT_PATH} ]] && PATH="${CONVERT_PATH}:${PATH}"

### CHECK PARAMETERS
if [[ -z ${1} ]] || [[ -z ${2} ]] || [[ -z ${3} ]]; then
	echo "ERROR: Lighter requires an image, a destination image name and a maximum size."
	exit 1
fi
if [[ ! -f ${1} ]]; then
	echo "ERROR: \"${1}\" file does not exist."
	exit 1
fi
if [[ ! ${3} =~ ^[0-9]+$ ]]; then
	echo "ERROR: Maximum size must be a number (integer)."
	exit 1
fi

### GET THE PARAMETERS
img_src=${1}	# THE ORIGINAL IMAGE
img_dst=${2}	# THE DESTINATION IMAGE
img_max=${3}	# THE MAXIMUM SIZE IN ko
max_size=$((img_max * 1024))

### INIT VARIABLES
quality=50
padding=25

### BEGIN LOOP
while : ; do
	### CONVERT IMAGE TO SPECIFIED QUALITY
	convert ${img_src} -quality ${quality} ${img_dst}
	### GET SIZE
	size=$(wc -c < ${img_dst})
	printf "quality [%3u] size [%3uko]\n" ${quality} $((size / 1024))
	### CHECK SIZE
	if [[ ${size} -eq ${max_size} ]]; then
		exit 0
	elif [[ ${size} -lt ${max_size} ]]; then
		((quality += padding))
	else
		((quality -= padding))
	fi
	[[ ${quality} -lt 1 ]] && quality=1
	[[ ${quality} -gt 100 ]] && quality=100
	### SHARPEN PADDING
	[[ ${padding} -eq 0 ]] && break
	if [[ $((padding % 2)) -eq 1 ]] && [[ ${padding} -gt 1 ]]; then
		((padding = (padding / 2) + 1))
	else
		((padding /= 2))
	fi
done

### CORRECT IF NECESSARY
if [[ ${size} -gt ${max_size} ]]; then
	if [[ ${quality} -gt 1 ]]; then
		((quality--))
		${convert} ${img_src} -quality ${quality} ${img_dst}
		size=$(wc -c < ${img_dst})
		printf "quality [%3u] size [%3uko]\n" ${quality} $((size / 1024))
	else
		echo "ERROR: Cannot reduce image size enough."
		exit 1
	fi
fi
