---
title: "Static"
date: 2018-11-15T15:34:27+01:00
draft: false

rule: "Your website must be static."
---

### EXPLANATION

Dynamic websites require **additional calculation** when a user queries a page.
We think that in the overwhelming majority of website, this is not
indispensable.

### ALTERNATIVE

#### STATIC WEBSITE GENERATOR
There are mutliple ways to build a **static website**. I choosed to use a
**static website generator**. It allows me to handle every single part of the
website, design it exactly how I want to and also allows me, once the basic
architecture is done, to really simply add new pages.
I personally use [hugo](https://gohugo.io), it is **free**, **open-source**,
full of nice features and really fast. It also allows me to automatically
**minify** my website source code when I build it.

#### HANDLE RESEARCH
One of the main issue with static websites is **search engine**. If the user
wants to query a page, the **less clic** he has to reach the page the better
cause each clic requires more calculation and so pollutes.

##### CATEGORIZATION
A classical way to handle this would be to use **categories** to **sort your
pages**.  If your pages are well categorized, the user will find them quickly.
One issue to this solution would be **redundancy**. To give a good example, if
your website stores a list of movies by category, the movie "*Matrix"* would be
findable in the "*action*" category but also in the "*science-fiction*" one and
perhaps even in another one which is called **redundancy**. The movie is stored
**only one time**, but the little link to the movie is present in **multiple
times**. It is not a big issue (as it is a single line) but a lot redundancy can
make your website heavier.

##### CTRL-F
Another way, which would seems funny, would be to ask users to find the right
page using **ctrl-F** (search) into a long list of items. It's pretty fast and
handled by the **browser** itself. You just need to tell your users how to
search pages through your giant list. We can think that since the list can be
huge, the user will had to load a lot of useless informations. But if the giant
list is **cached** it will have to be load one time for multiple researches.

##### BOTH
Of course, you can use both **categorization** and **ctrl-F** systems in your
website to get a performant **searching system**.

#### HANDLE COMMENTS
First of all, for most of the websites, we think that comments are **not
indispensable**. Though, we think it can still be **usefull** for some and it
would be too bad to **forbid** it. So to handle **comments** in a static
website, the good old trick would be to create an **email form** just like in
the old websites. You'll have then to manage your emails and update your
website with the new comments.

Here's a form example in **HTML** and how it looks like:

	<form action="mailto:someone@example.com" method="post" enctype="text/plain">
		Name:<br>
		<input type="text" name="name"><br>
		E-mail:<br>
		<input type="text" name="mail"><br>
		Comment:<br>
		<textarea name="comment" cols="40" rows="5"></textarea><br><br>
		<input type="submit" value="Send">
		<input type="reset" value="Reset">
	</form>

<form action="mailto:someone@example.com" method="post" enctype="text/plain">
Name:<br>
<input type="text" name="name"><br>
E-mail:<br>
<input type="text" name="mail"><br>
Comment:<br>
<textarea name="comment" cols="40" rows="5"></textarea><br><br>
<input type="submit" value="Send">
<input type="reset" value="Reset">
</form>
