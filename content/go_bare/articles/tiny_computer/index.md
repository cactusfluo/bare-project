---
title: "My Tiny Computer"
date: 2018-11-07T11:54:32+01:00
draft: true

description: "What about having a really tiny computer with low consumption ?"
---

Another option to reduce computer usage consumption would be to change your
computer to a simpler one.

coucou

An important thing to notice is that buying a computer, even a Raspberry Pi or
a Beagle Board, is polluting. It's manufacturing requires a lot of metals, all
polluting a lot. Its travel to your home is also polluting. Getting a simpler
computer is a nice idea, but you have to thing first,
