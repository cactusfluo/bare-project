---
title: "Bare Articles"
date: 2018-11-02T20:01:08+01:00
draft: false
layout: "articles"

public: "everyone"
description: "The Go Bare Advices to reduce your environmental impact from
technology"
---

* * *

> &#x1f43b; &#x1f334; <br>
"Look for the bare necessities <br>
The simple bare necessities <br>
Forget about your worries and your strife <br>
I mean the bare necessities <br>
Old Mother Nature's recipes <br>
That brings the bare necessities of life"

* * *

Here's some good practices to change your lifestyle and reduce your
environmental impact from technology usage:
