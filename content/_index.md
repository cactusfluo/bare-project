---
title: ""
date: 2018-11-01T20:41:51+01:00
draft: false

icon: "&#x1f43b;"
---

> The Bare Necessities Project is a project which aims to promote and build a
greener and sounder web.
<br>&#x1f30d; &#x1f30f; &#x1f30e; 

* * *

At the **Bare Necessities Project**, we think that our current technology usage
**is not sustainable**. On one hand, we try to provide internet users and
developers **informations** about how to use or create it the **greener** and
**sounder** way. And on the other hand, we try to build ourself this web.
Developing [websites as side projects]({{< ref "/projects" >}}) and imaginating
how a bare internet would be.

### First thing first, what even is a green or a sound web ? 

For us, a **green** website is designed to consume the lowest energy possible.
Thanks to this, terminals (computer, smartphones etc.) consume less and may
**live longer**. It walks hand to hand with what we call a **sound** or
**ethic** website. It means that this is a website which keeps only the
**essential**. A website where the **content is more important than its dress**.
It is also a website which does not make money on your back by selling your
informations and/or your attention.  We'd like to call this a **bare** website.

### What can I do ?

We are still working on ways to **minimize your ecological impact** while using
technology.

The [Go Bare]({{< ref "/go_bare" >}}) section is made to help people reduce
their environmental impact from technology. It contains
[facts and numbers]({{< ref "/go_bare/facts" >}}) about technology pollution,
[articles]({{< ref "/go_bare/articles" >}}) on what **internet users** can do to
be actors in the pollution reduction and a
[Green Dev Guide]({{< ref "/go_bare/guide" >}}) destinated to developers who
want to develop the **greener** way.

You can also visit the [Bare Projects]({{< ref "/projects" >}}) section which
lists our side bare website projects but also other websites handled by people
who cares about the same things. These websites require less energy from your
computer and even from the datacenters that hosts them. As the most important
part of the technology pollution comes from its production, increasing its
lifespan by preserving it is one of the most green act you can do.
By doing this, you also allow your terminal to consume less. You'll have to
recharge it less frequently and it can be a gain on your electricity bill (which
is also an green act !).

<!-- LABEL UNAVAILABLE -->
The [Bare Label]({{< ref "/label" >}}) section will describe how we conceive a
really **green** and **sound** website. It will explain what the label means and
list the rules your site must follow to get it.

<!-- LABEL AVAILABLE -->
<!--
The [Bare Label]({{< ref "/label" >}}) section describes how we conceive a
really **green** and **sound** website. It explains what the label means and
lists the rules your site must follow to get it.
(To get Bare Labelled or to be listed as a Bare Project, please 
[contact us]({{< ref "/contact" >}})).
-->

* * *

> We are currently working on a paper/essay to describe our ideal web and how it 
would change our habits and consumption and drastically reduce our environmental
impact linked to technology. We will publish it when it will be available.
